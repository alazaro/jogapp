import angular from 'angular';

export default ($location, $window, Restangular, Flash, apiBaseUri) => {
  'ngInject';

  return Restangular.withConfig((RestangularConfigurer) => {
    RestangularConfigurer.setBaseUrl(apiBaseUri);
    RestangularConfigurer.setRequestSuffix('/');
    RestangularConfigurer.addErrorInterceptor((response) => {
      if (response.status === 401) {
        $location.path('/login');
        return false;
      }
      return true;
    });
    RestangularConfigurer.addFullRequestInterceptor(
      (element, operation, route, url, headers, params, httpConfig) => {
        const jwt = $window.localStorage.getItem('token');
        if (jwt && !angular.isString(headers.Authorization)) {
          headers.Authorization = `Bearer ${jwt}`;
        }
        return {
          element,
          params,
          headers,
          httpConfig,
        };
      });
    RestangularConfigurer.addRequestInterceptor((elem, operation) => {
      if (operation === 'remove') {
        return undefined;
      }
      return elem;
    });
  });
};

export const TokenRefresh = ($http, apiBaseUri, $window) => {
  'ngInject';

  return {
    refresh() {
      const request = $http({
        url: `${apiBaseUri}/auth/token-refresh/`,
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        data: { token: $window.localStorage.getItem('token') },
      });
      request.then((response) => {
        $window.localStorage.setItem('token', response.data.token);
      },
        () => ($window.localStorage.removeItem('token')),
      );
      return request;
    },
  };
};
