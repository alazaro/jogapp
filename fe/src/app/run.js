export default (editableOptions, $rootScope, $location, $window,
    AuthenticationService, TokenRefresh) => {
  'ngInject';

  editableOptions.theme = 'bs3';

  $rootScope.$on('tokenHasExpired', () => {
    $window.localStorage.removeItem('token');
    $location.path('login');
  });
  $rootScope.$on('$routeChangeStart', (event, next) => {
    if (!AuthenticationService.isAuthenticated() && next.originalPath !== '/registration') {
      $location.path('login');
    }
  });
  setInterval(() => {
    if ($window.localStorage.getItem('token')) {
      TokenRefresh.refresh().then(null, () => {
        $location.path('/login');
      });
    }
  }, 30000);
};
