export const apiBaseUri = 'http://localhost:8000';

export const config = ($routeProvider, $httpProvider, jwtOptionsProvider, $locationProvider) => {
  'ngInject';

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false,
  });
  $routeProvider.otherwise({ redirectTo: '/jogs' });
  jwtOptionsProvider.config({
    whiteListedDomains: ['localhost:8000'],
  });
};
