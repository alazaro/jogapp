export default ($location, $scope, AuthenticationService) => {
  'ngInject';

  $scope.$location = $location;
  $scope.as = AuthenticationService;
};
