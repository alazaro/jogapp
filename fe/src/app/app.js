import 'babel-polyfill';
import 'moment';
import 'bootstrap-css';
import angular from 'angular';
import 'angular-bootstrap';
import 'angular-route';
import 'angular-jwt';
import 'lodash';
import 'restangular';
import 'angular-flash-alert';
import { apiBaseUri, config } from './config';
import JogsAPI, { TokenRefresh } from './api';
import run from './run';
import RootController from './root';
import registration from './views/registration';
import login from './views/login';
import jogs from './views/jogs';
import users from './views/users';
import report from './views/report';


angular.module('jogapp', [
  'ngRoute',
  'ngFlash',
  'angular-jwt',
  'restangular',
  'ui.bootstrap',
  registration,
  login,
  jogs,
  users,
  report,
])
  .controller('RootController', RootController)
  .config(config)
  .factory('JogsAPI', JogsAPI)
  .factory('TokenRefresh', TokenRefresh)
  .run(run)
  .constant('apiBaseUri', apiBaseUri);
