import 'angular-xeditable/dist/css/xeditable.css';
import 'angular-xeditable';
import angular from 'angular';
import templateList from './users.html';


export default angular.module('jogapp.users', ['ngRoute', 'xeditable', 'ngFlash', 'jogapp.login'])
  .config(['$routeProvider', ($routeProvider) => {
    $routeProvider
      .when('/users', {
        template: templateList,
      });
  }])

  .controller('UsersCtrl', function UsersCtrl(Flash, JogsAPI, AuthenticationService) {
    'ngInject';

    const vm = this;

    vm.users = [];

    vm.userListLoading = false;

    vm.initData = () => {
      vm.loadUsers().then(vm.loadGroups);
    };

    vm.groups = [];

    vm.loadGroups = () => JogsAPI.all('groups').getList().then(
      data => (vm.groups = data),
      error => Flash.create('danger', error),
    );

    vm.loadUsers = () => JogsAPI.all('users').getList().then(
      (data) => {
        vm.users = data;
      },
      error => Flash.create('danger', error),
    );

    vm.updateUser = (index, data) => {
      let user = vm.users[index] || {};
      user = Object.assign(user, data);
      if (typeof user.groups === 'number') {
        user.groups = [user.groups];
      } else if (typeof user.groups[0] === 'string') {
        return 'Cant save';
      }
      if (user.id) {
        user.save().then((object) => {
          Flash.create('info', 'User updated');
          user = Object.assign(user, object);
          vm.users[index] = object;
        },
          error => Flash.create('danger', `Couldn't update user: ${error.data}')`),
        );
      } else {
        JogsAPI.all('users').post(user).then((object) => {
          Flash.create('info', 'User created');
          user = Object.assign(user, object);
          vm.users[index] = object;
        },
          error => Flash.create('danger', error),
        );
      }
    };

    vm.as = AuthenticationService;

    vm.removeUser = (index) => {
      const user = vm.users[index];
      if (user && user.id) {
        JogsAPI.one('users', user.id).remove().then(() => {
          vm.users.splice(index, 1);
        },
          error => Flash.create('danger', error.data),
        );
      } else {
        vm.users.splice(index, 1);
      }
    };

    vm.addUser = () => {
      vm.inserted = {
        username: '',
        email: '',
        groups: null,
        password: '',
      };
      vm.users.push(vm.inserted);
    };

    vm.validatePassword = (data, userId) => {
      if (!userId && !data) {
        return 'The password cannot be empty';
      }
      return true;
    };

    vm.validateGroup = (data) => {
      if (typeof data === 'string') {
        return 'This field cannot be empty';
      }
      return true;
    };

    vm.validateField = (data) => {
      if (data === undefined) return 'This cannot be empty';
      return true;
    };
  })
  .name;
