import 'lodash';
import 'angular-xeditable/dist/css/xeditable.css';
import 'angular-xeditable';
import angular from 'angular';
import template from './report.html';


export default angular.module('jogapp.report', ['ngRoute', 'ngFlash'])
  .config(['$routeProvider', ($routeProvider) => {
    $routeProvider
      .when('/reports', {
        template,
      });
  }])

  .controller('ReportCtrl', function ReportCtrl(Flash, JogsAPI) {
    'ngInject';

    const vm = this;

    vm.reports = {};

    vm.reportsLoading = false;

    vm.showYear = week => week.substr(0, 4);
    vm.showWeek = week => week.substr(4, week.length);

    vm.loadReports = () => {
      vm.reportsLoading = true;
      JogsAPI.all('jogs').get('per_week').then((data) => {
        vm.reportsLoading = false;
        vm.reports = data.reports;
      },
        error => Flash.create('danger', error),
      );
    };
  })
  .name;
