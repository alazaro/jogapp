import angular from 'angular';
import logoutTemplate from './logout.html';
import template from './login.html';
import './login.css';

const login = angular.module('jogapp.login', ['ngRoute'])
  .config(['$routeProvider', ($routeProvider) => {
    $routeProvider.when('/login', {
      template,
      controller: 'UserCtrl',
    });
  }])

  .controller('UserCtrl',
    function UserCtrl($scope, $location, $window, UserService, AuthenticationService) {
      'ngInject';

      const vm = this;
      vm.logIn = (username, password) => {
        vm.error = null;
        UserService.logIn(username, password).then((data) => {
          if (data.data.token) {
            $window.localStorage.setItem('token', data.data.token);
            $location.path('/');
          }
        },
          (data) => {
            vm.error = [].concat(...Object.values(data.data));
          });
      };

      vm.isTabActive = tabName => $location.path() === tabName;
    });

login.factory('AuthenticationService', ['$window', 'jwtHelper', ($window, jwtHelper) => {
  const getToken = () => {
    const token = $window.localStorage.token;
    return (token && jwtHelper.decodeToken(token)) || null;
  };
  const auth = {
    isAdmin() {
      const token = getToken();
      if (!token) return false;
      return token.groups.indexOf('admins') > -1;
    },
    isManager() {
      const token = getToken();
      if (!token) return false;
      return token.groups.indexOf('user_managers') > -1;
    },
    isAuthenticated() {
      const token = $window.localStorage.token;
      if (!token) {
        return false;
      }
      return !jwtHelper.isTokenExpired(token);
    },
  };

  return auth;
}]);

login.factory('UserService', function UserService(
  $window, $http, apiBaseUri, $httpParamSerializerJQLike, jwtHelper, $location) {
  'ngInject';

  const vm = this;

  vm.logIn = (username, password) => $http({
    url: `${apiBaseUri}/auth/login/`,
    method: 'POST',
    data: $httpParamSerializerJQLike({
      username,
      password,
    }),
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  });

  vm.logout = () => {
    $window.localStorage.removeItem('token');
    $location.path('/login');
  };

  vm.getUserData = () => {
    const token = $window.localStorage.token;
    if (!token) {
      return {};
    }
    const data = jwtHelper.decodeToken(token);

    return {
      username: data.username,
      userGroups: data.groups,
    };
  };

  vm.getUsername = () => vm.getUserData().username;

  return vm;
})

  .component('logout', {
    controller: function Logout(UserService) {
      'ngInject';

      return {
        logout: UserService.logout,
        username: UserService.getUsername(),
        groups: UserService.getUserData().userGroups,
      };
    },

    template: logoutTemplate,
  });

export default login.name;
