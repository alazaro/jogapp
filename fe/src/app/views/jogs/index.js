import moment from 'moment';
import 'angular-xeditable/dist/css/xeditable.css';
import 'angular-xeditable';
import angular from 'angular';
import templateList from './jogs.html';


export default angular.module('jogapp.jogs', ['ngRoute', 'xeditable', 'ngFlash'])
  .config(['$routeProvider', ($routeProvider) => {
    $routeProvider
      .when('/jogs', {
        template: templateList,
      });
  }])

  .controller('JogsCtrl', function JogsCtrl(Flash, JogsAPI, AuthenticationService) {
    'ngInject';

    const vm = this;

    vm.jogs = [];

    vm.jogListLoading = false;

    vm.initData = () => {
      if (vm.as.isAdmin()) {
        vm.loadUsers().then(
          vm.getJogs,
        );
      } else {
        vm.getJogs();
      }
    };

    vm.getJogs = (filter) => {
      let params = {};
      const dateFormat = 'YYYY-MM-DD HH:mm:ss';
      if (filter) {
        const { start, end } = filter;
        params = {
          start_time_0: (start && moment(start).format(dateFormat)) || '',
          start_time_1: (end && moment(end).format(dateFormat)) || '',
        };
      }
      vm.jogListLoading = true;
      JogsAPI.all('jogs').getList(
        params,
      ).then((data) => {
        vm.jogs = data;
        vm.jogListLoading = false;
      },
      error => Flash.create('danger', `Couldn't load jogs: ${error.data}')`),
      );
    };

    vm.loadUsers = () => JogsAPI.all('users').getList().then(
      (data) => {
        vm.users = data;
        data.forEach(u => (vm.usernames[u.id] = u.username));
      },
      error => Flash.create('danger', `Couldn't load users: ${error.data}')`),
    );

    vm.users = [];

    vm.usernames = {};

    vm.updateJog = (index, data) => {
      let jog = vm.jogs[index] || {};
      jog = Object.assign(jog, data);
      jog.start_time = moment(jog.start_time).format();
      if (jog.id) {
        jog.save().then((object) => {
          Flash.create('info', 'Jog updated');
          jog = Object.assign(jog, object);
          vm.jogs[index] = object;
        },
          error => Flash.create('danger', `Couldn't update jog: ${error.data}')`),
        );
      } else {
        JogsAPI.all('jogs').post(jog).then((object) => {
          Flash.create('info', 'Jog created');
          jog = Object.assign(jog, object);
          vm.jogs[index] = object;
        },
          error => Flash.create('danger', `Couldn't create jog: ${error.data}')`),
        );
      }
    };

    vm.as = AuthenticationService;

    vm.removeJog = (index) => {
      const jog = vm.jogs[index];
      if (jog && jog.id) {
        JogsAPI.one('jogs', jog.id).remove().then(() => {
          vm.jogs.splice(index, 1);
        },
          error => Flash.create('danger', `Couldn't remove jog: ${error.data}')`),
        );
      } else {
        vm.jogs.splice(index, 1);
      }
    };

    vm.addJog = () => {
      vm.inserted = {
        id: '',
        start_time: new Date().toISOString(),
        duration: 0,
        distance: 0,
      };
      vm.jogs.push(vm.inserted);
    };

    vm.validateField = (data) => {
      if (data === undefined) return 'This cannot be empty';
      if (data < 1) return 'This has to be greater than 0';
      return true;
    };

    vm.displayTime = time => moment(time).calendar();

    vm.displayAverageSpeed = jog =>
      jog.average_speed || Math.round((jog.distance * 360) / jog.duration) / 100 || 0;
  })
  .name;
