import angular from 'angular';
import template from './registration.html';


const registrationController = function registrationController(JogsAPI, $window, $location) {
  'ngInject';

  const vm = this;
  vm.error = null;

  vm.register = (username, password1, password2, email) => {
    const newUser = {
      username,
      password1,
      password2,
      email,
    };
    JogsAPI.all('/auth/registration/').post(newUser)
      .then((data) => {
        if (data.token) {
          $window.localStorage.setItem('token', data.token);
          $location.path('/');
        }
      },
        (data) => {
          vm.error = [].concat(...Object.values(data.data));
        },
      );
  };
};


export default angular.module('jogapp.registration',
  ['ngRoute'],
).controller('registrationController', registrationController)

.config(['$routeProvider', ($routeProvider) => {
  $routeProvider
    .when('/registration', {
      template,
    });
}])
.name;
