# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-15 23:34
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='JoggingTime',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.DateTimeField()),
                ('distance', models.FloatField()),
                ('duration', models.FloatField()),
                ('average_speed', models.FloatField(blank=True, null=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='jogs', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
