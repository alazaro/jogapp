from django.conf import settings
from rest_framework_jwt.utils import jwt_payload_handler


def user_is_admin(user):
    return user.groups.filter(name=settings.ADMIN_GROUP).exists()


def user_is_manager(user):
    return user.groups.filter(name=settings.USER_MANAGER_GROUP).exists()


def jwt_payload(user):
    payload = jwt_payload_handler(user)
    payload.update(groups=list(user.groups.values_list('name', flat=True)))
    return payload
