from django.db import models


class JoggingTime(models.Model):
    start_time = models.DateTimeField(blank=False, null=False)
    distance = models.FloatField(blank=False, null=False)
    duration = models.FloatField(blank=False, null=False)
    average_speed = models.FloatField(null=True, blank=True)
    owner = models.ForeignKey('auth.User', related_name='jogs')

    def save(self, *args, **kwargs):
        self.calculate_average_speed()
        return super().save(*args, **kwargs)

    def calculate_average_speed(self):
        if self.distance and self.duration:
            self.average_speed = int(self.distance * 360 / self.duration) / 100
