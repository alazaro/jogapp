from django.contrib import admin

from .models import JoggingTime


admin.site.register(JoggingTime)
