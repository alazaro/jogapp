from collections import defaultdict

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group, User
from django_filters import rest_framework as filters
from rest_framework import serializers
from rest_framework.decorators import list_route
from rest_framework.permissions import (
    BasePermission,
    IsAuthenticated,
    SAFE_METHODS,
)
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import JoggingTime
from .utils import user_is_admin, user_is_manager


class UserSerializer(serializers.ModelSerializer):
    groups = serializers.StringRelatedField(many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'groups')

    def to_internal_value(self, data):
        groups = data.get('groups')
        if groups and not isinstance(groups, list):
            data['groups'] = [groups]
        password = data.get('password')
        if password:
            data['password'] = make_password(password)
        return data

    def to_representation(self, obj):
        repr = super().to_representation(obj)
        repr.pop('password')
        return repr


class JogFilter(filters.FilterSet):
    start_time = filters.DateTimeFromToRangeFilter()

    class Meta:
        model = JoggingTime
        fields = ('start_time',)


class JogSerializer(serializers.ModelSerializer):
    class Meta:
        model = JoggingTime
        fields = '__all__'
        read_only_fields = ('average_speed',)
        extra_kwargs = {'owner': {'required': False}}

    def validate_owner(self, owner):
        request_user = self.context['request'].user
        if user_is_admin(request_user) and owner:
            return owner

    def save(self):
        request = self.context['request']
        if not user_is_admin(request.user):
            return super().save(owner=request.user)
        return super().save()


class JogPermissions(BasePermission):
    def has_object_permission(self, request, view, obj):
        return user_is_admin(request.user) or obj.owner == request.user


class JogViewSet(ModelViewSet):
    model = JoggingTime
    serializer_class = JogSerializer
    permission_classes = (IsAuthenticated, JogPermissions,)
    filter_class = JogFilter

    def get_queryset(self):
        if user_is_admin(self.request.user):
            return self.model.objects.all()
        return self.request.user.jogs.all()

    @list_route()
    def per_week(self, request):
        jogs = self.get_queryset()
        week_dict = {}
        for j in jogs:
            week = j.start_time.strftime('%Y%W')
            if week not in week_dict:
                week_dict[week] = {
                    'speed': [],
                    'distance': [],
                    'count': 0,
                }
            week_dict[week]['speed'] += [j.average_speed]
            week_dict[week]['distance'] += [j.distance]
            week_dict[week]['count'] += 1

        return Response({'reports': {
            str(w): {
                'average_speed': sum(
                    week_dict[w]['speed']) / week_dict[w]['count'],
                'average_distance': sum(
                    week_dict[w]['distance']) / week_dict[w]['count']
            } for w in week_dict
        }})


class UserPermissions(BasePermission):
    def has_permission(self, request, view):
        return user_is_admin(request.user) or user_is_manager(request.user)


class UserViewSet(ModelViewSet):
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.prefetch_related('groups')
    permission_classes = (UserPermissions,)


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'


class ReadOnlyPermission(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class GroupViewSet(ModelViewSet):
    model = Group
    serializer_class = GroupSerializer
    queryset = Group.objects.all()
    permission_classes = (ReadOnlyPermission,)
