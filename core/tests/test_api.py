from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase


class RegistrationTestCase(TestCase):
    def test_register_user(self):
        response = self.client.post(
            reverse('rest_register'),
            {
                'username': 'John',
                'password1': 'pass123',
                'password2': 'pass123',
                'email': 'john@example.com',
            })
        self.assertEqual(response.status_code, 201)

    def test_register_user_wrong_password(self):
        response = self.client.post(
            reverse('rest_register'),
            {
                'username': 'John',
                'password1': 'pass123',
                'password2': 'pass',
                'email': 'john@example.com',
            })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json()['non_field_errors'],
            ['The two password fields didn\'t match.']
        )

    def test_register_no_password(self):
        response = self.client.post(
            reverse('rest_register'),
            {
                'username': 'John',
                'email': 'john@example.com',
            })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json()['password1'],
            ['This field is required.']
        )

    def test_register_no_email(self):
        response = self.client.post(
            reverse('rest_register'),
            {
                'username': 'John',
                'password1': 'pass123',
                'password2': 'pass123',
            })
        self.assertEqual(response.status_code, 201)

    def test_register_already_existing_username(self):
        response = self.client.post(
            reverse('rest_register'),
            {
                'username': 'John',
                'password1': 'pass123',
                'password2': 'pass123',
            })
        self.assertEqual(response.status_code, 201)

        response = self.client.post(
            reverse('rest_register'),
            {
                'username': 'John',
                'password1': 'pass123',
                'password2': 'pass123',
            })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json()['username'],
            ['A user with that username already exists.']
        )


class LoginTestCase(TestCase):
    def test_register_and_login_correct(self):
        response = self.client.post(
            reverse('rest_register'),
            {
                'username': 'John',
                'password1': 'pass123',
                'password2': 'pass123',
            })
        self.assertEqual(response.status_code, 201, response.json())
        response = self.client.post(
            reverse('rest_login'),
            {
                'username': 'John',
                'password': 'pass123',
            }
        )
        self.assertEqual(response.status_code, 200, response.json())
        self.assertIn('token', response.json())


class UsersTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('john', password='1234')

    def test_get_current_user_info(self):
        token = self.client.post(
            reverse('rest_login'),
            {
                'username': 'john',
                'password': '1234',
            }
        ).json()['token']

        response = self.client.get(reverse('rest_user_details'))
        self.assertEqual(response.status_code, 401, response.json())

        auth_header = {'HTTP_AUTHORIZATION': 'Bearer {0}'.format(token)}
        response = self.client.get(reverse('rest_user_details'), **auth_header)
        self.assertEqual(response.status_code, 200, response.json())
