from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.routers import SimpleRouter
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_swagger.views import get_swagger_view

from core import views

schema_view = get_swagger_view(title='JogApp API')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('rest_auth.urls')),
    url(r'^auth/token-refresh/', refresh_jwt_token),
    url(r'^auth/registration/', include('rest_auth.registration.urls')),
    url(r'^docs/$', schema_view),
]

router = SimpleRouter()
router.register(
    'jogs', views.JogViewSet, base_name='jogging_times')
router.register(
    'users', views.UserViewSet)
router.register(
    'groups', views.GroupViewSet)

urlpatterns += router.urls
